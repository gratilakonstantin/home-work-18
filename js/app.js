function currentTime(id, value) {
    document.getElementById(id).innerHTML = value;
    document.getElementById(id).style.backgroundImage = `url(../assets/images/${value}.jpeg)`;
}

setInterval(function () {

    const hours = new Date().getHours() < 10 ? `0${new Date().getHours()}` : new Date().getHours();
    const hourFirst = hours.toString().slice(0, -1);
    const hourSecond = hours.toString().slice(-1);
    const minutes = new Date().getMinutes() < 10 ? `0${new Date().getMinutes()}` : new Date().getMinutes();
    const minuteFirst = minutes.toString().slice(0, -1);
    const minuteSecond = minutes.toString().slice(-1);
    const seconds = new Date().getSeconds() < 10 ? `0${new Date().getSeconds()}` : new Date().getSeconds();
    const secondFirst = seconds.toString().slice(0, -1);
    const secondSecond = seconds.toString().slice(-1);


    const hours1ValueDOM = document.getElementById('hours1').innerHTML;
    const hours2ValueDOM = document.getElementById('hours2').innerHTML;

    const minutes1ValueDOM = document.getElementById('minutes1').innerHTML;
    const minutes2ValueDOM = document.getElementById('minutes2').innerHTML;


    const second1ValueDOM = document.getElementById('seconds1').innerHTML;
    const second2ValueDOM = document.getElementById('seconds2').innerHTML;


    if (hours1ValueDOM !== hourFirst) {
        currentTime('hours1', hourFirst);
    }

    if (hours2ValueDOM !== hourSecond) {
        currentTime('hours2', hourSecond);
    }

    if (minutes1ValueDOM !== minuteFirst) {
        currentTime('minutes1', minuteFirst);
    }

    if (minutes2ValueDOM !== minuteSecond) {
        currentTime('minutes2', minuteSecond);
    }

    if (second1ValueDOM !== secondFirst) {
        currentTime('seconds1', secondFirst);
    }

    if (second2ValueDOM !== secondSecond) {
        currentTime('seconds2', secondSecond);
    }
}, 1000);


